package com.biblezon.memorymatch;

public class GameData {

	private int pos;
	private String word;

	public GameData(int pos, String word) {
		this.pos = pos;
		this.word = word;
	}

	public int getItemPos() {
		return pos;
	}

	public String getWord() {
		return word;
	}
}
