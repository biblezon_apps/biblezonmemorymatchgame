package com.biblezon.memorymatch.utils;

public interface OnMovementListener {
	public abstract void onMovement();
}
