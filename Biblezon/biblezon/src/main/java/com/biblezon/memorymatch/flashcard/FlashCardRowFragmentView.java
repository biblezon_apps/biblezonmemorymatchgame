package com.biblezon.memorymatch.flashcard;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.biblezon.memorymatch.R;
import com.biblezon.memorymatch.model.CatholicWordsModel;

/**
 * Showing product to GUI on we use this fragment to show data
 * 
 * @author Anshuman
 *
 */
public class FlashCardRowFragmentView extends Fragment {

	private CatholicWordsModel CatholicItem;

	/**
	 * This method return instance of the same fragment
	 * 
	 * @return
	 */
	public static Fragment getInstance() {
		/* Create new instance of fragment */
		Fragment fragment = new FlashCardRowFragmentView();
		return fragment;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * android.support.v4.app.Fragment#onCreateView(android.view.LayoutInflater,
	 * android.view.ViewGroup, android.os.Bundle)
	 */
	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.product_page_view, container,
				false);
		/* Image view for set product image on GUI */
		ImageView catholicImage = (ImageView) view
				.findViewById(R.id.catholicImage);
		TextView catholicWord = (TextView) view.findViewById(R.id.catholicWord);
		/* Set image on image view */
		catholicImage.setImageResource(CatholicItem.getCatholicWordsImage());
		catholicWord.setText(CatholicItem.getCatholicWordsText());
		return view;
	}

	/**
	 * Set product image on GUI @Setter
	 * 
	 * @param productImage
	 */
	public void setDummyItem(CatholicWordsModel CatholicItem) {
		this.CatholicItem = CatholicItem;
	}
}
