package com.biblezon.memorymatch.memorymatch;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Chronometer;
import android.widget.CompoundButton;
import android.widget.GridLayout;
import android.widget.ToggleButton;

import com.biblezon.memorymatch.R;
import com.biblezon.memorymatch.control.MediaControlManager;
import com.biblezon.memorymatch.utils.AppDefaultUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * Created by Anshuman on 4/7/2015.
 */
public class BoardFragment extends Fragment {

    public static final String DIFF_KEY = "difficulty";

    /**
     * Debugging Tag
     */
    String TAG = BoardFragment.class.getSimpleName();

    private List<Integer> mChatholicImages;

    private Random rand;

    private ToggleButton activeCard;

    private Drawable cardBack;

    private int pairsFound;
    private int pairCount;

    private Chronometer timer;
    private boolean isFirstFound = false, isSecondFound = false;
    int count = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        rand = new Random();
        initializeCards();
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.board_fragment, container,
                false);

        timer = (Chronometer) rootView.findViewById(R.id.timer);

        return rootView;
    }

    protected void setCardBack(Drawable cardBack) {
        this.cardBack = cardBack;
    }

    protected void setPairCount(int pairCount) {
        this.pairCount = pairCount;

    }

    private void initializeCards() {

        // mChatholicImages = new ArrayList<Integer>();
        // mChatholicImages.add(R.drawable.altar_bells);
        // mChatholicImages.add(R.drawable.altar_server);
        // mChatholicImages.add(R.drawable.altar);
        // mChatholicImages.add(R.drawable.angels);
        // mChatholicImages.add(R.drawable.baptism);
        // mChatholicImages.add(R.drawable.baptismal_font);
        // mChatholicImages.add(R.drawable.bible);
        // mChatholicImages.add(R.drawable.blessed_sacrament);
        // mChatholicImages.add(R.drawable.chasuble);
        // mChatholicImages.add(R.drawable.confession);
        // mChatholicImages.add(R.drawable.confessional);
        // mChatholicImages.add(R.drawable.baptismal_font2);
        // mChatholicImages.add(R.drawable.crucifix);
        // mChatholicImages.add(R.drawable.eucharist_cup);
        // mChatholicImages.add(R.drawable.holy_mary);
        // mChatholicImages.add(R.drawable.holy_water_font);
        // mChatholicImages.add(R.drawable.host);
        // mChatholicImages.add(R.drawable.jesus);
        // mChatholicImages.add(R.drawable.jhsskif);
        // mChatholicImages.add(R.drawable.last_supper);
        // mChatholicImages.add(R.drawable.monstrance);
        // mChatholicImages.add(R.drawable.nun);
        // mChatholicImages.add(R.drawable.pews);
        // mChatholicImages.add(R.drawable.pope);
        // mChatholicImages.add(R.drawable.rosary);
        // /* Repeat */
        // mChatholicImages.add(R.drawable.aspergillum);
        // mChatholicImages.add(R.drawable.burse);
        // mChatholicImages.add(R.drawable.crosier);
        // mChatholicImages.add(R.drawable.corporal);
        // mChatholicImages.add(R.drawable.crotalus);
        // mChatholicImages.add(R.drawable.altar_candles);
        // mChatholicImages.add(R.drawable.aspersorium);

        mChatholicImages = new ArrayList<Integer>();
        mChatholicImages.add(R.drawable.pope_francis);
        mChatholicImages.add(R.drawable.bible);
        mChatholicImages.add(R.drawable.our_lady_of_guadaloupe);
        mChatholicImages.add(R.drawable.mother_theresa);
        mChatholicImages.add(R.drawable.liturgical_calendar);
        mChatholicImages.add(R.drawable.holy_oils);
        mChatholicImages.add(R.drawable.holy_family);
        mChatholicImages.add(R.drawable.dove);
        mChatholicImages.add(R.drawable.cross);
        mChatholicImages.add(R.drawable.ascension);
        mChatholicImages.add(R.drawable.holy_orders);
        mChatholicImages.add(R.drawable.matrimony);
        mChatholicImages.add(R.drawable.anointing_of_the_sick);
        mChatholicImages.add(R.drawable.confirmation);
        mChatholicImages.add(R.drawable.eucharist);
        mChatholicImages.add(R.drawable.last_supper);
        mChatholicImages.add(R.drawable.angels);
        mChatholicImages.add(R.drawable.holy_water_font);
        mChatholicImages.add(R.drawable.altar_bells);
        mChatholicImages.add(R.drawable.altar_server);
        mChatholicImages.add(R.drawable.altar);
        mChatholicImages.add(R.drawable.pews);
        mChatholicImages.add(R.drawable.confessional);
        mChatholicImages.add(R.drawable.confession);
        mChatholicImages.add(R.drawable.baptism);
        mChatholicImages.add(R.drawable.rosary);
        mChatholicImages.add(R.drawable.crucifix);
        mChatholicImages.add(R.drawable.holy_mary);
        mChatholicImages.add(R.drawable.jesus);
        mChatholicImages.add(R.drawable.paschal_candle);
        mChatholicImages.add(R.drawable.baptismal_font);
        mChatholicImages.add(R.drawable.host);
        mChatholicImages.add(R.drawable.eucharist_cup);
        mChatholicImages.add(R.drawable.chalice);
        mChatholicImages.add(R.drawable.monstrance);
        mChatholicImages.add(R.drawable.advent_wreath);

    }

    @Override
    public void onResume() {
        super.onResume();
        timer.start();
    }

    @Override
    public void onPause() {
        super.onPause();
        timer.stop();
    }

    // protected void setDifficulty(Score.Difficulty difficulty) {
    // this.difficulty = difficulty;
    // }

    /**
     * Return a card image that has not yet been added to the deck, removing it
     * from the pool of available cards.
     */
    private int getAvailableCardFace() {
        return mChatholicImages.remove(rand.nextInt(mChatholicImages.size()));
    }

    /**
     * Create the pool of card faces, randomly selected. Each one is added twice
     * to ensure exactly two copies of each card appear on the board
     */
    protected List<Integer> selectCardPool(int uniqueCardCount) {
        List<Integer> cardFaces = new ArrayList<Integer>();
        for (int i = 0; i < uniqueCardCount; i++) {
            int cardFace = getAvailableCardFace();
            cardFaces.add(cardFace);
            cardFaces.add(cardFace);

        }
        Collections.shuffle(cardFaces);
        return cardFaces;
    }

    protected int calculateCardWidth(int columns, double scalingFactor) {
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return (int) (size.x / (columns + scalingFactor));
    }

    protected void addButtonsToGrid(GridLayout grid, int columns, int rows,
                                    int cardWidth) {

        final List<Integer> cardFaces = selectCardPool(pairCount);
        for (int i = 0; i < columns * rows; i++) {
            final ToggleButton button = new ToggleButton(getActivity());
            // Wanted to use a style here but couldn't get it working
            button.setTextOff("");
            button.setTextOn("");
            button.setBackground(cardBack);
            button.setChecked(false);
            button.setId(i);
            button.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView,
                                             boolean isChecked) {
                    // if (!isFirstFound || !isSecondFound) {
                    if (isChecked && (!isFirstFound || !isSecondFound)) {
                        if (isFirstFound && !isSecondFound) {
                            isSecondFound = true;
                            resetValues();
                        }
                        if (!isFirstFound) {
                            isFirstFound = true;

                        }
                        flipToBackAnimation(
                                button,
                                getActivity().getResources().getDrawable(
                                        cardFaces.get(button.getId())));
                        AppDefaultUtils.showLog(TAG, "button.getId() :"
                                + button.getId());
                        AppDefaultUtils.showLog(
                                TAG,
                                "cardFaces.get(button.getId() :"
                                        + cardFaces.get(button.getId()));

                        String fileName = getResources().getResourceName(
                                cardFaces.get(button.getId())).replaceAll(
                                "com.app.biblezon:drawable/", "");
                        AppDefaultUtils.showLog(TAG, "NAME :" + fileName);

                        MediaControlManager.getInstance(getActivity())
                                .startPlay(fileName);

                        // button.setBackground(getActivity().getResources()
                        // .getDrawable(cardFaces.get(button.getId())));
                        // checkAgainstActiveCard(button);
                    } else {
                        button.setBackground(cardBack);
                        button.setEnabled(true);
                    }

                }

            });
            grid.addView(button, cardWidth, cardWidth);
        }
    }

    protected void checkAgainstActiveCard(final ToggleButton newFlip) {
        if (activeCard != null) {

            isFirstFound = true;
            isSecondFound = true;
            resetValues();
            AppDefaultUtils.showLog(TAG, "activeCard :"
                    + activeCard.getBackground().getConstantState());
            AppDefaultUtils.showLog(TAG, "newFlip :"
                    + newFlip.getBackground().getConstantState());
            // Found a pair
            if (activeCard.getBackground().getConstantState()
                    .equals(newFlip.getBackground().getConstantState())) {

                // Pair matched
                activeCard.setEnabled(false);
                newFlip.setEnabled(false);
                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        newFlip.setVisibility(View.INVISIBLE);
                        activeCard.setVisibility(View.INVISIBLE);
                        activeCard = null;

                        isFirstFound = true;
                        isSecondFound = true;
                        resetValues();
                    }
                }, 400);

                pairsFound++;
                if (pairsFound == pairCount) {
                    endGame();
                }

            } else { // Did not find a pair
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            isFirstFound = true;
                            isSecondFound = true;
                            resetValues();
                            if (activeCard != null) {
                                activeCard.toggle();
                                activeCard.setEnabled(true);
                                activeCard = null;
                            }

                            newFlip.setChecked(false);
                            newFlip.setBackground(cardBack);
                            newFlip.setEnabled(true);
                        } catch (NullPointerException e) {
                            // TODO: handle exception
                            e.printStackTrace();
                        }

                    }
                }, 350);
            }
        } else { // First card to be flipped
            activeCard = newFlip;
            activeCard.setEnabled(false);
        }
    }

    // private void saveNewScore(Score.Difficulty difficulty) {
    // SharedPreferences prefs =
    // getActivity().getSharedPreferences(SettingsActivity.SETTINGS,
    // Context.MODE_PRIVATE);
    // Score newScore = new Score(timer.getText().toString(),
    // prefs.getString(SettingsActivity.NAME, "--"),
    // new Date(System.currentTimeMillis()), difficulty);
    // newScore.save();
    // }

    private void endGame() {
        timer.stop();
        // saveNewScore(difficulty);
        ((MemoryMatchGameScreen) getActivity()).showEndGameNavDialog(timer
                .getText().toString());
    }

    private void resetValues() {
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub
                isFirstFound = false;
                isSecondFound = false;
            }
        }, 200);
    }

    /**
     * @param button
     * @param drawable
     */
    private void flipToBackAnimation(final ToggleButton button,
                                     final Drawable drawable) {
        final Animator in = AnimatorInflater.loadAnimator(getActivity(),
                R.animator.card_flip_left_in);
        in.setTarget(button);

        Animator.AnimatorListener flipOutListener = new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                button.setBackground(drawable);
                checkAgainstActiveCard(button);
                in.start();
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        };

        Animator out = AnimatorInflater.loadAnimator(getActivity(),
                R.animator.card_flip_left_out);
        out.setTarget(button);
        out.addListener(flipOutListener);
        out.start();

    }
}
