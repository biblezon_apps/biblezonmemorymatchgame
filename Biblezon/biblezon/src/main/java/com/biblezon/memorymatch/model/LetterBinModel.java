package com.biblezon.memorymatch.model;

/**
 * letter bin model
 * 
 * @author Shruti
 * 
 */
public class LetterBinModel {
	String letter;
	boolean correct;
	boolean isEmpty;
	boolean isBlasted;
	public boolean isBlasted() {
		return isBlasted;
	}

	public void setBlasted(boolean isBlasted) {
		this.isBlasted = isBlasted;
	}

	boolean shouldAnimateOut;
	boolean shouldAnimateIn;

	public boolean isShouldAnimateOut() {
		return shouldAnimateOut;
	}

	public void setShouldAnimateOut(boolean shouldAnimateOut) {
		this.shouldAnimateOut = shouldAnimateOut;
	}

	public boolean isShouldAnimateIn() {
		return shouldAnimateIn;
	}

	public void setShouldAnimateIn(boolean shouldAnimateIn) {
		this.shouldAnimateIn = shouldAnimateIn;
	}

	public boolean isEmpty() {
		return isEmpty;
	}

	public void setEmpty(boolean isEmpty) {
		this.isEmpty = isEmpty;
	}

	public String getLetter() {
		return letter;
	}

	public void setLetter(String letter) {
		this.letter = letter;
	}

	public boolean isCorrect() {
		return correct;
	}

	public void setCorrect(boolean correct) {
		this.correct = correct;
	}
}
