package com.biblezon.memorymatch;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.biblezon.memorymatch.utils.GameManager;

public class DashBoard extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dashboard);

		Button game1 = (Button) findViewById(R.id.game1);
		game1.setTypeface(Typeface.createFromAsset(getAssets(), "comicbd.ttf"));
		Button game2 = (Button) findViewById(R.id.game2);
		game2.setTypeface(Typeface.createFromAsset(getAssets(), "comicbd.ttf"));

		TextView howToPlay = (TextView) findViewById(R.id.how_to_play);
		howToPlay.setTypeface(Typeface.createFromAsset(getAssets(),
				"comicbd.ttf"));
		howToPlay.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				// open how to play screen...
				getFragmentManager().beginTransaction()
						.replace(R.id.container, new HowToPlayFrag())
						.addToBackStack(null).commit();
			}
		});
	}

	@Override
	protected void onResume() {
		super.onResume();
		updateTopScore();
	}

	public void game1(View v) {
		// Open Game 1
		Intent intent = new Intent(DashBoard.this, KidsGame.class);
		intent.putExtra("whichgame", "game1");
		startActivity(intent);
	}

	public void game2(View v) {
		// Open Game 2
		Intent intent = new Intent(DashBoard.this, KidsGame.class);
		intent.putExtra("whichgame", "game2");
		startActivity(intent);
	}

	private void updateTopScore() {
		TextView score = (TextView) findViewById(R.id.best_score);
		score.setTypeface(Typeface.createFromAsset(getAssets(), "comicbd.ttf"));
		score.setText("Top Score \n"
				+ new GameManager(DashBoard.this).getGameTopScore() + " Move");
	}
}
