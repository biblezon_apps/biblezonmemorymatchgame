package com.biblezon.memorymatch.preference;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class ScoreManager {
	// Shared Preferences
	private SharedPreferences pref;
	// Editor for Shared preferences
	private Editor editor;
	// Context
	private Context mContext;
	// make private static instance of Sessionmanager class
	private static ScoreManager mScoreManager;

	/**
	 * getInstance method is used to initialize SessionManager singelton
	 * instance
	 * 
	 * @param context
	 *            context instance
	 * @return Singelton session manager instance
	 */
	public static ScoreManager getInstance(Context context) {
		if (mScoreManager == null) {
			mScoreManager = new ScoreManager(context);
		}
		return mScoreManager;
	}

	// Constructor
	@SuppressLint("CommitPrefEdits")
	private ScoreManager(Context context) {
		this.mContext = context;
		pref = mContext.getSharedPreferences(PreferenceHelper.PREFERENCE_NAME,
				PreferenceHelper.PRIVATE_MODE);
		editor = pref.edit();
	}

	/**
	 * Update Memory Match in preferences .
	 * 
	 * @param newScore
	 */
	public void updateMemoryMatchScore(int newScore) {
		// Storing Score in pref
		editor.putInt(PreferenceHelper.PREFERENCE_SCORE_MEMORYMATCHGAME,
				newScore);
		// commit changes
		editor.commit();
	}

	/**
	 * Update Name Match in preferences .
	 * 
	 * @param newScore
	 */
	public void updateNameMatchScore(int newScore) {
		// Storing Score in pref
		editor.putInt(PreferenceHelper.PREFERENCE_SCORE_NAME_MATCH, newScore);
		// commit changes
		editor.commit();
	}

	/**
	 * Update SpellIt in preferences .
	 * 
	 * @param newScore
	 */
	public void updateSpellItScore(int newScore) {
		// Storing Score in pref
		editor.putInt(PreferenceHelper.PREFERENCE_SCORE_SPELL_IT, newScore);
		// commit changes
		editor.commit();
	}

	/**
	 * Update TapIt in preferences .
	 * 
	 * @param newScore
	 */
	public void updateTapItScore(int newScore) {
		// Storing Score in pref
		editor.putInt(PreferenceHelper.PREFERENCE_SCORE_TAP_IT, newScore);
		// commit changes
		editor.commit();
	}

	/**************************
	 * Get All Score
	 * ***************************/

	/**
	 * Get Memory Match in preferences .
	 * 
	 */
	public Integer GetHighestMemoryMatchScore() {
		// Get Score Back in pref
		return pref
				.getInt(PreferenceHelper.PREFERENCE_SCORE_MEMORYMATCHGAME, 0);
	}

	/**
	 * Get Name Match in preferences .
	 * 
	 */
	public Integer GetHighestNameMatchScore() {
		// Get Score Back in pref
		return pref.getInt(PreferenceHelper.PREFERENCE_SCORE_NAME_MATCH, 0);
	}

	/**
	 * get SpellIt in preferences .
	 * 
	 */
	public Integer GetHighestSpellItScore() {
		// Get Score Back in pref
		return pref.getInt(PreferenceHelper.PREFERENCE_SCORE_SPELL_IT, 0);
	}

	/**
	 * Get TapIt in preferences .
	 * 
	 */
	public Integer GetHighestTapItScore(String newScore) {
		// Get Score Back in pref
		return pref.getInt(PreferenceHelper.PREFERENCE_SCORE_TAP_IT, 0);
	}

	/**
	 * Clear session details
	 * */
	public void RemoveAllStoredSroceUser() {
		// Clearing all data from Shared Preferences
		editor.clear();
		editor.commit();
	}

}
