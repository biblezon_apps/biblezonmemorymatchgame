package com.biblezon.memorymatch;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.view.DragEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.DragShadowBuilder;
import android.view.View.OnDragListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.biblezon.memorymatch.control.HeaderViewManager;
import com.biblezon.memorymatch.control.MediaControlManager;
import com.biblezon.memorymatch.control.PlaySound;
import com.biblezon.memorymatch.ihelper.AlertDialogClickListener;
import com.biblezon.memorymatch.preference.ScoreManager;
import com.biblezon.memorymatch.utils.AppDefaultUtils;
import com.biblezon.memorymatch.utils.AppDialogUtils;
import com.biblezon.memorymatch.utils.AppHelper;

/**
 * Name Match Screen View
 * 
 * @author Anshuman
 * 
 */
public class NameMatchScreen extends Activity implements OnTouchListener,
		OnDragListener {
	/**
	 * Activity Instance
	 */
	private Activity mActivity;
	/**
	 * @String Debugging TAG
	 */
	private String TAG = NameMatchScreen.class.getSimpleName();
	/**
	 * Word Text Layout
	 */
	private RelativeLayout firstWordView, secondWordView, thirdWordView,
			forthWordView, firthWordView, sixthWordView, seventhWordView,
			eighthWordView;
	/**
	 * Answer Views
	 */
	private RelativeLayout firstAnswerView, secondAnswerView, thirdAnswerView,
			forthAnswerView, fifthAnswerView, sixthAnswerView,
			seventhAnswerView, eighthAnswerView;
	/**
	 * Array of Images and image view
	 */
	final int[] imageViews = { R.id.firstWordImage, R.id.secondWordImage,
			R.id.thirdWordImage, R.id.forthWordImage, R.id.fifthWordImage,
			R.id.sixthWordImage, R.id.sevenththWordImage, R.id.eighthWordImage };
	/* Answer view array */
	final int[] answerViews = { R.id.firstAnswerView, R.id.secondAnswerView,
			R.id.thirdAnswerView, R.id.forthAnswerView, R.id.fifthAnswerView,
			R.id.sixthAnswerView, R.id.seventhAnswerView, R.id.eighthAnswerView };
	/* game Images Array */
	// final int[] images = { R.drawable.advent_wreath, R.drawable.alb,
	// R.drawable.altar, R.drawable.altar_bells, R.drawable.altar_candles,
	// R.drawable.ambry, R.drawable.book, R.drawable.cassock };
	// /* Image Name Array */
	// final String[] imageName = { AppHelper.NAME_MATCH_WORD_ADVENT_WREATH,
	// AppHelper.NAME_MATCH_WORD_ALB, AppHelper.NAME_MATCH_WORD_ALTAR,
	// AppHelper.NAME_MATCH_WORD_ALTAR_BELLS,
	// AppHelper.NAME_MATCH_WORD_ALTAR_CANDLES,
	// AppHelper.NAME_MATCH_WORD_AMBRY, AppHelper.NAME_MATCH_WORD_BOOK,
	// AppHelper.NAME_MATCH_WORD_CASSOCK };
	/* Word test view array */
	final int[] wordTextViews = { R.id.firstWordTextView,
			R.id.secondWordTextView, R.id.thirdWordTextView,
			R.id.forthWordTextView, R.id.firthWordTextView,
			R.id.sixthWordTextView, R.id.seventhWordTextView,
			R.id.eighthWordTextView };
	/* drayView Array */
	final int[] dragViewIds = { R.id.firstWordView, R.id.secondWordView,
			R.id.thirdWordView, R.id.forthWordView, R.id.firthWordView,
			R.id.sixthWordView, R.id.seventhWordView, R.id.eighthWordView };
	/**
	 * DragView Instance
	 */
	private View mDragView;
	/**
	 * App Success Count
	 */
	private int successCount = 0, currentScore = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.name_match_screen);
		initViews();
		headerViewManagement();
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		PlaySound.getInstance(mActivity).startPlay();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		PlaySound.getInstance(mActivity).stopPlay();
	}

	/**
	 * manages header view
	 */
	private void headerViewManagement() {
		HeaderViewManager.getInstance().InitializeHeaderView(mActivity);
		HeaderViewManager.getInstance().setHeading(true,
				mActivity.getResources().getString(R.string.name_match));
	}

	private void initViews() {
		mActivity = this;
		initializeIdsOfWordTextViews();
		initializeidsOfWordAnswerViews();
		assignDragListener();
		showWordImagesOnGUI();
		setWordTextOnView();
	}

	/**
	 * initialize Word Text View ids
	 */
	private void initializeIdsOfWordTextViews() {

		firstWordView = (RelativeLayout) findViewById(R.id.firstWordView);
		secondWordView = (RelativeLayout) findViewById(R.id.secondWordView);
		thirdWordView = (RelativeLayout) findViewById(R.id.thirdWordView);
		forthWordView = (RelativeLayout) findViewById(R.id.forthWordView);
		firthWordView = (RelativeLayout) findViewById(R.id.firthWordView);
		sixthWordView = (RelativeLayout) findViewById(R.id.sixthWordView);
		seventhWordView = (RelativeLayout) findViewById(R.id.seventhWordView);
		eighthWordView = (RelativeLayout) findViewById(R.id.eighthWordView);
	}

	/**
	 * initialize id of Word Answer View
	 */
	private void initializeidsOfWordAnswerViews() {
		// TODO Auto-generated method stub
		firstAnswerView = (RelativeLayout) findViewById(R.id.firstAnswerView);
		secondAnswerView = (RelativeLayout) findViewById(R.id.secondAnswerView);
		thirdAnswerView = (RelativeLayout) findViewById(R.id.thirdAnswerView);
		forthAnswerView = (RelativeLayout) findViewById(R.id.forthAnswerView);
		fifthAnswerView = (RelativeLayout) findViewById(R.id.fifthAnswerView);
		sixthAnswerView = (RelativeLayout) findViewById(R.id.sixthAnswerView);
		seventhAnswerView = (RelativeLayout) findViewById(R.id.seventhAnswerView);
		eighthAnswerView = (RelativeLayout) findViewById(R.id.eighthAnswerView);
	}

	/**
	 * Assign Drag-drop listener on view
	 */
	private void assignDragListener() {
		firstWordView.setOnDragListener(this);
		secondWordView.setOnDragListener(this);
		thirdWordView.setOnDragListener(this);
		forthWordView.setOnDragListener(this);
		firthWordView.setOnDragListener(this);
		sixthWordView.setOnDragListener(this);
		seventhWordView.setOnDragListener(this);
		eighthWordView.setOnDragListener(this);

		firstAnswerView.setOnDragListener(this);
		secondAnswerView.setOnDragListener(this);
		thirdAnswerView.setOnDragListener(this);
		forthAnswerView.setOnDragListener(this);
		fifthAnswerView.setOnDragListener(this);
		sixthAnswerView.setOnDragListener(this);
		seventhAnswerView.setOnDragListener(this);
		eighthAnswerView.setOnDragListener(this);

		firstWordView.setOnTouchListener(this);
		secondWordView.setOnTouchListener(this);
		thirdWordView.setOnTouchListener(this);
		forthWordView.setOnTouchListener(this);
		firthWordView.setOnTouchListener(this);
		sixthWordView.setOnTouchListener(this);
		seventhWordView.setOnTouchListener(this);
		eighthWordView.setOnTouchListener(this);

	}

	/**
	 * Shuffle that original map
	 * 
	 * @param map
	 * @return
	 */
	public static <K, V> Map<K, V> shuffleMap(Map<K, V> map) {
		List<V> valueList = new ArrayList<V>(map.values());
		Collections.shuffle(valueList);
		Iterator<V> valueIt = valueList.iterator();
		Map<K, V> newMap = new HashMap<K, V>(map.size());
		for (K key : map.keySet()) {
			newMap.put(key, valueIt.next());
		}
		return newMap;
	}

	/**
	 * Show Images on GUI
	 */
	@SuppressLint("DefaultLocale")
	private void showWordImagesOnGUI() {
		Random rng = new Random();
		List<Integer> generated = new ArrayList<Integer>();
		for (int i = 0; i < 8; i++) {
			while (true) {
				Integer next = rng.nextInt(8);
				if (!generated.contains(next)) {
					generated.add(next);
					ImageView iv = (ImageView) findViewById(imageViews[i]);
					iv.setImageBitmap(AppDefaultUtils
							.getImageFromStorage(AppHelper.catholicImagesArray[next]));
					RelativeLayout mAnswerView = (RelativeLayout) findViewById(answerViews[i]);
					mAnswerView.setTag(AppDefaultUtils
							.getImageTAGFromPath(
									AppHelper.catholicImagesArray[next]
											.replaceAll("_", " ")).toString()
							.toUpperCase());
					break;
				}
			}
		}

	}

	/**
	 * Set Text Word on GUI
	 */
	private void setWordTextOnView() {
		Random rng = new Random();
		List<Integer> generated = new ArrayList<Integer>();
		for (int i = 0; i < 8; i++) {
			while (true) {
				Integer next = rng.nextInt(8);
				if (!generated.contains(next)) {
					generated.add(next);
					TextView iv = (TextView) findViewById(wordTextViews[i]);
					iv.setText(AppHelper.catholicNameArray[next].replaceAll(
							"_", " ").toUpperCase());
					RelativeLayout mdragView = (RelativeLayout) findViewById(dragViewIds[i]);
					mdragView.setTag(AppHelper.catholicNameArray[next]
							.replaceAll("_", " ").toUpperCase());
					break;
				}
			}
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.view.View.OnDragListener#onDrag(android.view.View,
	 * android.view.DragEvent)
	 */
	@Override
	public boolean onDrag(View v, DragEvent event) {
		// TODO Auto-generated method stub
		int action = event.getAction();
		switch (action) {
		case DragEvent.ACTION_DRAG_STARTED:
			break;
		case DragEvent.ACTION_DRAG_ENTERED:
			break;
		case DragEvent.ACTION_DRAG_EXITED:
			break;
		case DragEvent.ACTION_DROP:
			onDropOfView(event, v);
			break;
		case DragEvent.ACTION_DRAG_ENDED:
			onDropEnded(event);
			break;
		default:
			break;
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.view.View.OnTouchListener#onTouch(android.view.View,
	 * android.view.MotionEvent)
	 */
	@SuppressLint("ClickableViewAccessibility")
	@Override
	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
		if (event.getAction() == MotionEvent.ACTION_DOWN) {
			mDragView = v;
			DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(
					mDragView);
			mDragView.startDrag(null, shadowBuilder, v, 0);
			mDragView.setVisibility(View.INVISIBLE);

			return true;
		} else {
			return false;
		}
	}

	/**
	 * User drop his answer on GUI
	 * 
	 * @param event
	 * @param v
	 */
	private void onDropOfView(DragEvent event, View v) {
		View dragView = (View) event.getLocalState();
		ViewGroup owner = (ViewGroup) dragView.getParent();
		RelativeLayout containerAnswerView = (RelativeLayout) v;
		AppDefaultUtils.showLog(TAG, "Drag view tag :"
				+ dragView.getTag().toString());
		AppDefaultUtils.showLog(TAG, "Container view tag :"
				+ containerAnswerView.getTag().toString());

		if (dragView.getTag().toString()
				.equalsIgnoreCase(containerAnswerView.getTag().toString())) {
			try {
				MediaControlManager.getInstance(mActivity).startPlay(
						dragView.getTag().toString().replaceAll(" ", "_")
								.toLowerCase());
			} catch (Exception e) {
				e.printStackTrace();
				// TODO: handle exception
			}

			dragView.setVisibility(View.VISIBLE);
			owner.removeView(dragView);
			containerAnswerView.addView(dragView);
			successCount++;
			if (successCount == 8) {
				String scoreMsg = "";
				if (ScoreManager.getInstance(mActivity)
						.GetHighestNameMatchScore() == 0 || currentScore == 8) {
					scoreMsg = AppHelper.HIGHEST_SCORE
							+ currentScore
							+ "\n\n"
							+ mActivity.getResources().getString(
									R.string.onsuccess);
					ScoreManager.getInstance(mActivity).updateNameMatchScore(
							currentScore);
				} else if (currentScore > ScoreManager.getInstance(mActivity)
						.GetHighestNameMatchScore()) {
					scoreMsg = AppHelper.HIGHEST_SCORE_IS
							+ ScoreManager.getInstance(mActivity)
									.GetHighestNameMatchScore()
							+ AppHelper.YOUR_SCORE_IS
							+ currentScore
							+ "\n\n"
							+ mActivity.getResources().getString(
									R.string.onsuccess);
				} else {
					scoreMsg = AppHelper.HIGHEST_SCORE
							+ currentScore
							+ "\n\n"
							+ mActivity.getResources().getString(
									R.string.onsuccess);
					ScoreManager.getInstance(mActivity).updateNameMatchScore(
							currentScore);
				}
				AppDialogUtils.showMessageInfoWithOkButtonDialog(mActivity,
						scoreMsg, onGameComplete());
				// new TextToSpeechControl(mActivity)
				// .startTTS(mActivity.getResources().getString(
				// R.string.onsuccess_for_audio));
				new Handler().postDelayed(new Runnable() {

					@Override
					public void run() {
						MediaControlManager.getInstance(mActivity).startPlay(
								"congratulation");
					}
				}, 1000);
			}
		} else {
			Animation animation = AnimationUtils.loadAnimation(
					getApplicationContext(), R.anim.shaking_anim);
			containerAnswerView.startAnimation(animation);
			mDragView.setVisibility(View.VISIBLE);
			MediaControlManager.getInstance(mActivity).startPlay("no");
		}
		currentScore++;
		AppDefaultUtils.showLog(TAG, "successCount :" + successCount);

	}

	/**
	 * OnDrop Ended
	 * 
	 * @param event
	 */
	private void onDropEnded(DragEvent event) {
		if (!event.getResult()) {
			mDragView.setVisibility(View.VISIBLE);
		}

	}

	/**
	 * On Game Complete listner
	 * 
	 * @return
	 */
	private AlertDialogClickListener onGameComplete() {
		AlertDialogClickListener mListener = new AlertDialogClickListener() {

			@Override
			public void onClickOfAlertDialogPositive() {
				onBackPressed();
			}
		};
		return mListener;
	}
}
