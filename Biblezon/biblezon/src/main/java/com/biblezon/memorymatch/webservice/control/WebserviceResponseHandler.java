package com.biblezon.memorymatch.webservice.control;

import org.json.JSONObject;

import com.biblezon.memorymatch.ihelper.GlobalKeys;

/**
 * Web API Response handler
 * 
 * @author Anshuman
 * 
 */
public class WebserviceResponseHandler {
	/**
	 * Instance of WebserviceResponseHandler
	 */
	private static WebserviceResponseHandler mResponseHandlerInstance;
	/**
	 * Debugging TAG
	 */
	@SuppressWarnings("unused")
	private String TAG = WebserviceResponseHandler.class.getSimpleName();

	/**
	 * Provide instance of this class
	 * 
	 * @return
	 */
	public static WebserviceResponseHandler getInstance() {
		if (mResponseHandlerInstance == null) {
			mResponseHandlerInstance = new WebserviceResponseHandler();
		}
		return mResponseHandlerInstance;
	}

	private WebserviceResponseHandler() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Check Response Code of the API
	 * 
	 * @param mObject
	 * @return
	 */
	public boolean checkResponseCode(JSONObject mObject) {
		try {
			String mResponseCode = mObject.getString(GlobalKeys.RESPONSE_CODE);
			if (mResponseCode.equalsIgnoreCase(GlobalKeys.SUCCESS)) {
				return true;
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return false;
		}

		return false;
	}

	/**
	 * Check Response Message of the API
	 * 
	 * @param mObject
	 * @return
	 */
	public String getResponseMessage(JSONObject mObject) {
		try {
			String mResponseMessage = mObject
					.getString(GlobalKeys.RESPONSE_MESSAGE);
			return mResponseMessage;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return null;
		}

	}

	/**
	 * Check Response Code of the API
	 * 
	 * @param mObject
	 * @return
	 */
	public boolean checkVersionResponseCode(JSONObject mObject) {
		try {
			boolean mResponseCode = mObject.getBoolean(GlobalKeys.MSG_SUCCESS);
			if (mResponseCode) {
				return true;
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return false;
		}

		return false;
	}

}
