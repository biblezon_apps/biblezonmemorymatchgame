package com.biblezon.memorymatch;

import it.sephiroth.android.library.widget.HListView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.biblezon.memorymatch.adapter.LetterBinAdapter;
import com.biblezon.memorymatch.adapter.SpellWordAdapter;
import com.biblezon.memorymatch.control.HeaderViewManager;
import com.biblezon.memorymatch.control.MediaControlManager;
import com.biblezon.memorymatch.control.PlaySound;
import com.biblezon.memorymatch.ihelper.AlertDialogClickListener;
import com.biblezon.memorymatch.model.LetterBinModel;
import com.biblezon.memorymatch.model.SpellWordModel;
import com.biblezon.memorymatch.utils.AppDefaultUtils;
import com.biblezon.memorymatch.utils.AppDialogUtils;
import com.biblezon.memorymatch.utils.AppHelper;
import com.biblezon.memorymatch.utils.GlobalConfig;

/**
 * Spell it activity shows a image on the top and the player has to enter its
 * name correctly by picking letters from random letter bin
 * 
 * @author Shruti
 * 
 */
public class SpellItNewActivity extends Activity implements OnClickListener {

	/**
	 * layout inflater instance
	 */
	LayoutInflater inflater;
	/**
	 * zoom in and zoom out animation
	 */
	Animation zoom_in, zoom_out;
	/**
	 * dummy string
	 */
	ArrayList<String> required_string;
	ArrayList<Integer> required_image;
	// = new String[] { "APPLE MANGO ORANGE", "APPLE",
	// "APPLE ORANGE", "MANGO ORANGE", "ORANGE" };
	String current_word = "";
	/**
	 * horizontal Scroll view instance to show the empty spaces for words
	 */
	HListView first_word, second_word, third_word;
	/**
	 * Textview to show the letter count
	 */
	TextView letter_count;
	/**
	 * Debug tag for this class
	 */
	String TAG = SpellItNewActivity.class.getSimpleName();
	/**
	 * Image view for showing the puzzle image
	 */
	ImageView big_image;
	TextView bomb_image, suggestion_image;

	/**
	 * Grid view to show the letter bin
	 */
	GridView letter_bin_grid;

	/**
	 * Array list for Actual letters, predicted letters
	 */
	List<SpellWordModel> actualLetterList, predicted_list;

	/**
	 * ArrayList for letterbin
	 */
	ArrayList<LetterBinModel> letter_bin;

	/**
	 * hash map to map letter bin position and predicted letter position
	 */
	HashMap<Integer, Integer> predictedMappedLetterBin;
	int letter_bin_count = 18;
	int total_puzzel_letter = 0, current_index = 0;
	TextView skipText, erase_image;

	/**
	 * letter bin grid view adapter instance
	 */
	LetterBinAdapter letterBinAdapter;

	/**
	 * SpellWordAdapter instance
	 */
	SpellWordAdapter spellWordAdapter, secondSpellWordAdapter,
			thirdSpellWordAdapter;

	/**
	 * Activity instance
	 */
	Activity activity;

	ArrayList<Integer> unmatch_position,
			filledPosition = new ArrayList<Integer>();
	ArrayList<Integer> individualListCount;
	String all_alphabets = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	ArrayList<Integer> letterbin_pos = new ArrayList<Integer>();

	/**
	 * puzzle count
	 */
	int puzzle_count = GlobalConfig.no_of_question, current_puzzle = 1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.spell_it_layout);
		inflater = (LayoutInflater) this
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		initViews();
		assignClicks();
		getAllPuzzleWord();
		headerViewManagement();
		updateFieldValues();
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		PlaySound.getInstance(this).startPlay();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		PlaySound.getInstance(this).stopPlay();
	}

	/**
	 * manages header view
	 */
	private void headerViewManagement() {
		HeaderViewManager.getInstance().InitializeHeaderView(activity);
		HeaderViewManager.getInstance().setHeading(true,
				activity.getResources().getString(R.string.spell_it));
	}

	/**
	 * updates the puzzle empty list and letter bin according to the new puzzel
	 * words
	 */
	@SuppressLint("DefaultLocale")
	public void updateFieldValues() {
		if (required_string != null
				&& required_string.size() > (current_puzzle - 1)) {
			String Actualword = required_string.get(current_puzzle - 1)
					.replaceAll("_", "").toUpperCase();
			AppDefaultUtils.showLog(TAG, "Actualword :" + Actualword);
			makeActualLetterList(Actualword);
			makeRandomLetterBin(Actualword);
			if (required_image != null
					&& required_image.size() > (current_puzzle - 1)) {
				big_image.setImageResource(required_image
						.get(current_puzzle - 1));
				// big_image
				// .setImageBitmap(AppDefaultUtils
				// .getImageFromStorage(AppHelper.catholicImagesArray[current_puzzle
				// - 1]));
				String fileName = required_string.get(current_puzzle - 1);
				AppDefaultUtils.showLog(TAG, "Music file name :" + fileName);

				MediaControlManager.getInstance(this).startPlay(fileName);
			}
			changeCurrentLetterIndex();
		} else if (current_puzzle > puzzle_count) {
			// Toast.makeText(activity, "Hurray!! you have completed it.",
			// Toast.LENGTH_LONG).show();
			AppDialogUtils.showMessageInfoWithOkButtonDialog(
					SpellItNewActivity.this, SpellItNewActivity.this
							.getResources().getString(R.string.onsuccess),
					onGameComplete());
		}
	}

	/**
	 * assign grid view item click, hlistview item clicks
	 */
	private void assignClicks() {
		bomb_image.setOnClickListener(this);
		suggestion_image.setOnClickListener(this);
		letter_bin_grid.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View view,
					int position, long arg3) {
				String selected_letter = letter_bin.get(position).getLetter();
				if (unmatch_position != null && unmatch_position.size() > 0
						&& !letter_bin.get(position).isEmpty()
						&& !letter_bin.get(position).isBlasted()) {
					letterbin_pos.add(position);
					current_index = unmatch_position.get(0);
					filledPosition.add(current_index);
					unmatch_position.remove(0);
					if (predicted_list != null
							&& predicted_list.size() > current_index) {
						letter_bin.get(position).setEmpty(true);
						SpellWordModel model = new SpellWordModel();
						model.setLetter(selected_letter);
						predicted_list.set(current_index, model);
						predictedMappedLetterBin.put((current_index), position);
						changeLetterBinGridViewItems();
						changeHListViewItems();
						changeAvailabiltyOfBombImage();
						AppDefaultUtils.showLog(TAG, "current_index : "
								+ current_index + " : " + total_puzzel_letter);
						if (unmatch_position != null
								&& unmatch_position.size() > 0) {
							current_index = unmatch_position.get(0);
						}
						if (unmatch_position == null
								|| unmatch_position.size() == 0) {

							// Show some progress here
							new Handler().postDelayed(new Runnable() {
								@Override
								public void run() {
									matchActualLettersToPredicted();
								}
							}, 2000);
						}
					}
				}
			}
		});

		skipText.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				getAllPuzzleWord();
				updateFieldValues();
			}
		});

//		erase_image.setOnClickListener(new View.OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				try {
//					if (filledPosition != null && filledPosition.size() > 0) {
//						SpellWordModel predictedModel = new SpellWordModel();
//						predictedModel.setLetter("");
//						predicted_list.set(filledPosition.size() - 1,
//								predictedModel);
//						predictedMappedLetterBin.remove(filledPosition.size() - 1);
//						letter_bin.get(
//								letterbin_pos.get(letterbin_pos.size() - 1))
//								.setEmpty(false);
//						Collections.reverse(unmatch_position);
//						unmatch_position.add(filledPosition.get(filledPosition
//								.size() - 1));
//						Collections.reverse(unmatch_position);
//						letterbin_pos.remove(letterbin_pos.size() - 1);
//						filledPosition.remove(filledPosition.size() - 1);
//						changeLetterBinGridViewItems();
//						changeHListViewItems();
//						changeAvailabiltyOfBombImage();
//
//					}
//
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//			}
//		});
	}

	/**
	 * matches the actual letters to the predicted letters
	 */
	protected void matchActualLettersToPredicted() {
		current_index = -1;
		unmatch_position = new ArrayList<Integer>();
		filledPosition = new ArrayList<Integer>();
		for (int match_count = 0; match_count < total_puzzel_letter; match_count++) {
			if (actualLetterList
					.get(match_count)
					.getLetter()
					.equalsIgnoreCase(
							predicted_list.get(match_count).getLetter())) {
				// actualLetterList.remove(match_count);
				predicted_list.get(match_count).setCorrect(true);
				letter_bin.get(predictedMappedLetterBin.get(match_count))
						.setEmpty(true);
				filledPosition.add(match_count);
			} else {
				unmatch_position.add(match_count);
				predicted_list.get(match_count).setLetter("");
				if (letter_bin != null
						&& predictedMappedLetterBin != null
						&& predictedMappedLetterBin.size() > match_count
						&& letter_bin.size() > predictedMappedLetterBin
								.get(match_count)) {
					letter_bin.get(predictedMappedLetterBin.get(match_count))
							.setEmpty(false);
				}
			}
		}
		if (unmatch_position != null && unmatch_position.size() > 0) {
			current_index = unmatch_position.get(0);
		} else {
			current_index = 0;
			// Toast.makeText(activity, "Hurray!! you have done it.",
			// Toast.LENGTH_LONG).show();

			updateFieldValues();
			second_word.setVisibility(View.GONE);
			third_word.setVisibility(View.GONE);
		}
		changeLetterBinGridViewItems();
		changeHListViewItems();
	}

	/**
	 * change the three horizontal list values according to the puzzle words
	 */
	private void changeHListViewItems() {
		List<SpellWordModel> individualSubList = new ArrayList<SpellWordModel>();
		if (individualListCount != null && individualListCount.size() > 0) {

			AppDefaultUtils.showLog(TAG, "individualListCount.size() : "
					+ individualListCount.size());
			if (individualListCount.size() > 0) {
				individualSubList = new ArrayList<SpellWordModel>();
				individualSubList = predicted_list.subList(0,
						(individualListCount.get(0)));
				AppDefaultUtils.showErrorLog(TAG, "individualSubList :"
						+ individualSubList);
				spellWordAdapter.addUpdateDataIntoList(individualSubList);
				spellWordAdapter.notifyDataSetChanged();
			}
			if (individualListCount.size() > 1) {
				individualSubList = new ArrayList<SpellWordModel>();
				individualSubList = predicted_list.subList(
						(individualListCount.get(0)),
						(individualListCount.get(1)));
				secondSpellWordAdapter.addUpdateDataIntoList(individualSubList);
				secondSpellWordAdapter.notifyDataSetChanged();
				second_word.setVisibility(View.VISIBLE);
			}
			if (individualListCount.size() > 2) {
				individualSubList = new ArrayList<SpellWordModel>();
				individualSubList = predicted_list.subList(
						(individualListCount.get(1)),
						(individualListCount.get(2)));
				thirdSpellWordAdapter.addUpdateDataIntoList(individualSubList);
				thirdSpellWordAdapter.notifyDataSetChanged();
				second_word.setVisibility(View.VISIBLE);
				third_word.setVisibility(View.VISIBLE);
			}
		} else {
			spellWordAdapter.addUpdateDataIntoList(predicted_list);
			spellWordAdapter.notifyDataSetChanged();
		}
	}

	/**
	 * updates letter bin grid view items
	 */
	private void changeLetterBinGridViewItems() {
		letterBinAdapter.addUpdateDataIntoList(letter_bin);
		letterBinAdapter.notifyDataSetChanged();
	}

	/**
	 * change the text on the letter count textview
	 */
	protected void changeCurrentLetterIndex() {
		letter_count.setText(current_puzzle + "/" + puzzle_count);
		current_puzzle = current_puzzle + 1;
	}

	/**
	 * make a random list including the required letters
	 * 
	 * @param required_word
	 */
	@SuppressLint("DefaultLocale")
	private void makeRandomLetterBin(String required_word) {
		letter_bin = new ArrayList<LetterBinModel>();
		if (required_word != null && required_word.contains(" ")) {
			required_word = required_word.replace(" ", "");
		}
		if (required_word != null && required_word.length() > 0) {
			all_alphabets = removeAllMatchingLetters(all_alphabets,
					required_word);
			/** get remaining letter count and make a random list of that count */
			int remaining_letters = letter_bin_count - required_word.length();
			String random_list = "";
			if (remaining_letters > 0) {
				AppDefaultUtils.showLog(TAG, "remaining_letters : "
						+ remaining_letters);
				for (int remaining_count = 0; remaining_count < remaining_letters; remaining_count++) {
					Random r = new Random();
					// char c = (char) (r.nextInt(26) + 'a');
					char c = all_alphabets.charAt(r.nextInt(all_alphabets
							.length()));
					random_list = random_list + c;
					LetterBinModel binModel = new LetterBinModel();
					binModel.setLetter((c + "").toUpperCase());
					binModel.setCorrect(false);
					binModel.setShouldAnimateIn(true);
					letter_bin.add(binModel);
				}
			}

			Collections.shuffle(letter_bin);
		}
		changeLetterBinGridViewItems();
	}

	/**
	 * removes all the required letters from the alphabet set
	 * 
	 * @param alphabets
	 *            all alphabets
	 * @param required_word
	 *            puzzle word
	 * @return filtered alphabet set
	 */
	private String removeAllMatchingLetters(String alphabets,
			String required_word) {
		if (required_word != null && !required_word.isEmpty()) {
			for (int i = 0; i < required_word.length(); i++) {
				char c = required_word.charAt(i);
				LetterBinModel binModel = new LetterBinModel();
				binModel.setLetter(c + "");
				binModel.setShouldAnimateIn(true);
				binModel.setCorrect(true);
				letter_bin.add(binModel);
				if (alphabets != null && alphabets.contains(c + "")) {
					alphabets = alphabets.replace(c + "", "");
				}
			}
		}

		return alphabets;
	}

	/**
	 * make the actual letter list
	 * 
	 * @param required_letter
	 */
	@SuppressLint("DefaultLocale")
	private void makeActualLetterList(String required_word) {
		actualLetterList = new ArrayList<SpellWordModel>();
		predicted_list = new ArrayList<SpellWordModel>();
		unmatch_position = new ArrayList<Integer>();
		individualListCount = new ArrayList<Integer>();
		if (required_word != null && required_word.contains(" ")) {
			String[] splitted_string = required_word.split(" ");
			if (splitted_string.length == 1) {
				second_word.setVisibility(View.VISIBLE);
			} else {
				second_word.setVisibility(View.VISIBLE);
				third_word.setVisibility(View.VISIBLE);
			}

			int count = 0;
			for (int split_count = 0; split_count < splitted_string.length; split_count++) {
				count = count + splitted_string[split_count].length();
				individualListCount.add(count);
			}
			required_word = required_word.replace(" ", "");
		}

		current_word = required_word;
		AppDefaultUtils.showLog(TAG, " current_word : ____" + current_word);
		for (int letter_count = 0; letter_count < required_word.length(); letter_count++) {
			unmatch_position.add(letter_count);
			SpellWordModel model = new SpellWordModel();
			model.setLetter((required_word.charAt(letter_count) + "")
					.toUpperCase());
			actualLetterList.add(model);

			SpellWordModel predictedModel = new SpellWordModel();
			predictedModel.setLetter("");
			predicted_list.add(predictedModel);
		}
		total_puzzel_letter = required_word.length();
		changeHListViewItems();
	}

	/**
	 * initializes the fields views
	 */
	@SuppressLint("UseSparseArrays")
	private void initViews() {
		activity = this;
		first_word = (HListView) findViewById(R.id.first_word);
		letter_bin_grid = (GridView) findViewById(R.id.letter_bin_grid);
		second_word = (HListView) findViewById(R.id.second_word);
		third_word = (HListView) findViewById(R.id.third_word);
		letter_count = (TextView) findViewById(R.id.letter_count);
		big_image = (ImageView) findViewById(R.id.big_image);
		bomb_image = (TextView) findViewById(R.id.bomb_image);
		suggestion_image = (TextView) findViewById(R.id.suggestion_image);
		skipText = (TextView) findViewById(R.id.skipText);
		erase_image = (TextView) findViewById(R.id.erase_image);
		predictedMappedLetterBin = new HashMap<Integer, Integer>();
		letterBinAdapter = new LetterBinAdapter(activity);
		letter_bin_grid.setAdapter(letterBinAdapter);
		spellWordAdapter = new SpellWordAdapter(activity);
		first_word.setAdapter(spellWordAdapter);
		secondSpellWordAdapter = new SpellWordAdapter(activity);
		second_word.setAdapter(secondSpellWordAdapter);
		second_word.setVisibility(View.GONE);
		thirdSpellWordAdapter = new SpellWordAdapter(activity);
		third_word.setAdapter(thirdSpellWordAdapter);
		third_word.setVisibility(View.GONE);
		zoom_in = AnimationUtils.loadAnimation(this, R.anim.zoom_in);
		zoom_out = AnimationUtils.loadAnimation(this, R.anim.zoom_out);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.bomb_image:
			int incorrect_pos = -1;
			if (letter_bin != null && letter_bin.size() > 0) {
				for (int i = 0; i < letter_bin.size(); i++) {
					if (incorrect_pos < 0) {
						if (!letter_bin.get(i).isEmpty()
								&& !letter_bin.get(i).isCorrect()
								&& !letter_bin.get(i).isBlasted()) {
							letter_bin.get(i).setBlasted(true);
							incorrect_pos = incorrect_pos + 1;
							break;
						}
					}
				}
				changeLetterBinGridViewItems();
				changeAvailabiltyOfBombImage();
			}
			break;

		case R.id.suggestion_image:
			chooseMatchingLetterFromLetterBin();

			break;

		default:
			break;
		}
	}

	/**
	 * clicks the item of gridview which is matching to the current required
	 * letter
	 */
	private void chooseMatchingLetterFromLetterBin() {
		AppDefaultUtils.showLog(TAG, "current letter is : "
				+ "current_word.length() ____________ " + current_word.length()
				+ " _____  current_index : " + current_index);
		if (current_word != null && current_word.length() > current_index) {
			AppDefaultUtils.showLog(TAG, "current letter is : " + current_index
					+ " : " + current_word.charAt(current_index));
			String required_letter = current_word.charAt(current_index) + "";
			if (letter_bin != null && letter_bin.size() > 0) {
				int pos = 0;
				for (int i = 0; i < letter_bin.size(); i++) {
					if (!letter_bin.get(i).isEmpty()
							&& !letter_bin.get(i).isBlasted()) {
						if (letter_bin.get(i).getLetter()
								.equalsIgnoreCase(required_letter)) {
							AppDefaultUtils.showLog(TAG, "here____________");
							// letter_bin_grid.setItemChecked(10, true);
							letter_bin_grid.performItemClick(letter_bin_grid,
									i, 0);
							// letter_bin_grid.setSelection(i);
							break;
						} else {
							pos = pos + 1;
							AppDefaultUtils.showLog(TAG, "pos : " + pos);
							if (pos + current_index >= letter_bin.size()) {
								resetCurrentPuzzle();
							}
						}
					}
				}
			}
		}
	}

	/**
	 * change the state of bomb icon to enable and disable
	 */
	public void changeAvailabiltyOfBombImage() {
		int incorrect_pos = -1;
		if (letter_bin != null && letter_bin.size() > 0) {
			for (int i = 0; i < letter_bin.size(); i++) {
				if (incorrect_pos < 0) {
					if (!letter_bin.get(i).isEmpty()
							&& !letter_bin.get(i).isCorrect()
							&& !letter_bin.get(i).isBlasted()) {
						incorrect_pos = incorrect_pos + 1;
						break;
					}
				}
			}
			if (incorrect_pos == 0) {
				bomb_image.setEnabled(true);
				bomb_image.setClickable(true);
				// bomb_image.setBackgroundResource(R.drawable.ic_launcher);
				// bomb_image.setBackgroundResource(R.drawable.bomb_btn);
			} else {
				bomb_image.setEnabled(false);
				bomb_image.setClickable(false);
				// bomb_image.setBackgroundResource(R.drawable.back_arrow);
				// bomb_image.setBackgroundResource(R.drawable.bomb_btn);
			}
		}
	}

	/**
	 * reset the current puzzle
	 */
	public void resetCurrentPuzzle() {
		if (required_string != null
				&& required_string.size() > (current_puzzle - 2)) {
			AppDefaultUtils.showLog(TAG, "makeActualLetterList___2 :");
			makeActualLetterList(required_string.get(current_puzzle - 2));
			makeRandomLetterBin(required_string.get(current_puzzle - 2));
		}
		if (required_image != null
				&& required_image.size() > (current_puzzle - 2)) {
			big_image.setImageResource(required_image.get(current_puzzle - 2));

			// big_image
			// .setImageBitmap(AppDefaultUtils
			// .getImageFromStorage(AppHelper.catholicImagesArray[current_puzzle
			// - 2]));
		}
		current_index = 0;
	}

	/**
	 * get all puzzle words according to the puzzle count
	 */
	public void getAllPuzzleWord() {
		required_string = new ArrayList<String>();
		required_image = new ArrayList<Integer>();
		Random rng = new Random();
		for (int i = 0; i < puzzle_count; i++) {
			while (true) {
				Integer next_value = rng.nextInt(AppHelper.sizeOfChatholicData);
				if (AppHelper.catholicNameArray[next_value].length() < 10) {
					required_string
							.add(AppHelper.catholicNameArray[next_value]);
					required_image
							.add(AppHelper.catholicImagesDrawableArray[next_value]);
				}

				// iv.setImageBitmap(AppDefaultUtils
				// .getImageFromStorage(AppHelper.catholicImagesArray[next]));
				// RelativeLayout mAnswerView = (RelativeLayout)
				// findViewById(answerViews[i]);
				// mAnswerView.setTag(AppDefaultUtils
				// .getImageTAGFromPath(
				// AppHelper.catholicImagesArray[next])
				// .toString().replaceAll("_", " ").toUpperCase());

				break;
			}
		}
	}

	/**
	 * On Game Complete listner
	 * 
	 * @return
	 */
	private AlertDialogClickListener onGameComplete() {
		AlertDialogClickListener mListener = new AlertDialogClickListener() {

			@Override
			public void onClickOfAlertDialogPositive() {
				onBackPressed();
			}
		};
		return mListener;
	}
}
