package com.biblezon.memorymatch.memorymatch.tiles;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridLayout;
import android.widget.RelativeLayout;

import com.biblezon.memorymatch.R;
import com.biblezon.memorymatch.memorymatch.BoardFragment;

/**
 * Created by Magisus on 4/5/2015.
 */
public class SixBySixBoardFragment extends BoardFragment {

	public static final String TAG = SixBySixBoardFragment.class
			.getSimpleName();
	public static final int GRID_HEIGHT = 6;
	public static final int GRID_WIDTH = 6;
	public static final int PAIR_COUNT = GRID_HEIGHT * GRID_WIDTH / 2;

	private Resources res;

	private GridLayout grid;

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		View rootView = super.onCreateView(inflater, container,
				savedInstanceState);

		res = getActivity().getResources();
		RelativeLayout baseLayout = (RelativeLayout) getActivity()
				.findViewById(R.id.bottomLayout);
		baseLayout.setBackgroundResource(R.drawable.base_blue_background);

		grid = (GridLayout) rootView.findViewById(R.id.boardLayout);
		grid.setRowCount(GRID_HEIGHT);

		setCardBack(res.getDrawable(R.drawable.memory_match_default));
		setPairCount(PAIR_COUNT);
		// setDifficulty(Score.Difficulty.MEDIUM);

		addButtonsToGrid(grid, GRID_WIDTH, GRID_HEIGHT,
				calculateCardWidth(GRID_WIDTH, 1.3));
		return rootView;
	}
}
