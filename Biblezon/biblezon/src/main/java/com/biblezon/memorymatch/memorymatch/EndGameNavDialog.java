package com.biblezon.memorymatch.memorymatch;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

import com.biblezon.memorymatch.R;


/**
 * Created by Magisus on 4/12/2015.
 */
public class EndGameNavDialog extends DialogFragment {

	public static final String TAG = "EndGameNavDialog";

	private static final String TIME_ARG = "TIME";

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle("You win!");
		builder.setCancelable(false);

		builder.setMessage("Your time:\n" + getArguments().getString(TIME_ARG));

		builder.setPositiveButton(getActivity().getString(R.string.play_again),
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						getActivity().finish();
						EndGameNavDialog.this.startActivity(new Intent(
								getActivity(), MemoryMatchGameScreen.class));
					}
				});

		return builder.create();
	}

	static EndGameNavDialog newInstance(String time) {
		EndGameNavDialog dialog = new EndGameNavDialog();

		Bundle args = new Bundle();
		args.putString(TIME_ARG, time);
		dialog.setArguments(args);
		return dialog;
	}
}
