package com.biblezon.memorymatch.flashcard.adapter;

import java.util.ArrayList;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.biblezon.memorymatch.flashcard.FlashCardRowFragmentView;
import com.biblezon.memorymatch.model.CatholicWordsModel;

/**
 * Page view adapter to show data on the page view (product image on page view)
 * 
 * @author Anshuman
 *
 */

public class PageViewFragmentStatePagerAdapter extends
		FragmentStatePagerAdapter {
	/**
	 * List which content page view data
	 */
	private ArrayList<CatholicWordsModel> mCatholicFlashCardArray;

	/**
	 * Constructor of this adapter class
	 * 
	 * @param fm
	 * @param mCatholicFlashCardArray
	 * @param mPageViewDataList
	 */
	public PageViewFragmentStatePagerAdapter(FragmentManager fm,
			ArrayList<CatholicWordsModel> mCatholicFlashCardArray) {
		super(fm);
		this.mCatholicFlashCardArray = mCatholicFlashCardArray;
	}

	/**
	 * Update new page list on GUI
	 * 
	 * @param mupdatePageViewData
	 */
	public void updatePagerImage(
			ArrayList<CatholicWordsModel> mCatholicFlashCardArray) {
		this.mCatholicFlashCardArray = mCatholicFlashCardArray;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.support.v4.view.PagerAdapter#getCount()
	 */
	@Override
	public int getCount() {
		return mCatholicFlashCardArray.size();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.support.v4.app.FragmentStatePagerAdapter#getItem(int)
	 */
	@Override
	public Fragment getItem(int position) {
		// Create a new instance of the fragment and return it.
		FlashCardRowFragmentView sampleFragment = (FlashCardRowFragmentView) FlashCardRowFragmentView
				.getInstance();
		sampleFragment.setDummyItem(mCatholicFlashCardArray.get(position));
		return sampleFragment;
	}

	/**
	 * This method is only gets called when we invoke
	 * {@link #notifyDataSetChanged()} on this adapter. Returns the index of the
	 * currently active fragments. There could be minimum two and maximum three
	 * active fragments(suppose we have 3 or more fragments to show). If there
	 * is only one fragment to show that will be only active fragment. If there
	 * are only two fragments to show, both will be in active state.
	 * PagerAdapter keeps left and right fragments of the currently visible
	 * fragment in ready/active state so that it could be shown immediate on
	 * swiping. Currently Active Fragments means one which is currently visible
	 * one is before it and one is after it.
	 *
	 * @param object
	 *            Active Fragment reference
	 * @return Returns the index of the currently active fragments.
	 */
	@Override
	public int getItemPosition(Object object) {
		return POSITION_NONE;
	}
}
