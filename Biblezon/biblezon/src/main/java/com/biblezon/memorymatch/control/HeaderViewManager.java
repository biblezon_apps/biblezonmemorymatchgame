package com.biblezon.memorymatch.control;

import android.app.Activity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.biblezon.memorymatch.R;
import com.biblezon.memorymatch.utils.AppDefaultUtils;

/**
 * Manage Header of the application
 * 
 * 
 * @author Anshuman
 * 
 */
public class HeaderViewManager {

	/**
	 * Instance of this class
	 */
	public static HeaderViewManager mHeaderManagerInstance;
	/**
	 * Debugging TAG
	 */
	private String TAG = HeaderViewManager.class.getSimpleName();

	/**
	 * Header View Instance
	 */
	private RelativeLayout headerLeftView;
	private TextView headerHeadingText;
	@SuppressWarnings("unused")
	private ImageView headerLeftImage;

	/**
	 * Instance of Header View Manager
	 * 
	 * @return
	 */
	public static HeaderViewManager getInstance() {
		if (mHeaderManagerInstance == null) {
			mHeaderManagerInstance = new HeaderViewManager();
		}
		return mHeaderManagerInstance;
	}

	/**
	 * Initialize Header View
	 * 
	 * @param mActivity
	 * @param mView
	 * @param headerViewClickListener
	 */
	public void InitializeHeaderView(Activity mActivity) {
		if (mActivity != null) {
			headerLeftView = (RelativeLayout) mActivity
					.findViewById(R.id.headerLeftView);
			headerHeadingText = (TextView) mActivity
					.findViewById(R.id.headerTextView);
			headerLeftImage = (ImageView) mActivity
					.findViewById(R.id.headerLeftViewImage);
			manageClickOnViews(mActivity);
		}

	}

	/**
	 * ManageClickOn Header view
	 * 
	 * @param mActivity
	 * 
	 * @param headerViewClickListener
	 */
	private void manageClickOnViews(final Activity mActivity) {
		// Click on Header Left View
		headerLeftView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				mActivity.onBackPressed();
			}
		});
	}

	/**
	 * Set Heading View Text
	 * 
	 * @param isVisible
	 * @param headingStr
	 */
	public void setHeading(boolean isVisible, String headingStr) {
		if (headerHeadingText != null) {
			if (isVisible) {
				headerHeadingText.setVisibility(View.VISIBLE);
				headerHeadingText.setText(headingStr);
			} else {
				headerHeadingText.setVisibility(View.GONE);
			}
		} else {
			AppDefaultUtils.showLog(TAG, "Header Heading Text View is null");
		}
	}
}
