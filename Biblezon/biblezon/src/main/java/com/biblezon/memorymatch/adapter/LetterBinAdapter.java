package com.biblezon.memorymatch.adapter;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.biblezon.memorymatch.R;
import com.biblezon.memorymatch.SpellItActivity;
import com.biblezon.memorymatch.model.LetterBinModel;

/**
 * show letter bin grid on GUI
 * 
 * @author Shruti
 * 
 */
public class LetterBinAdapter extends BaseAdapter {
	/**
	 * letter bin list
	 */
	private ArrayList<LetterBinModel> mLetterBinList;
	/**
	 * Context object
	 */
	private Context context;
	@SuppressWarnings("unused")
	private Activity activity;
	@SuppressWarnings("unused")
	private LayoutInflater mLayoutInflater;
	/**
	 * zoom in and zoom out animation
	 */
	Animation zoom_in, zoom_out;

	/**
	 * Debugging TAG
	 */
	private String TAG = LetterBinAdapter.class.getSimpleName();
	SpellItActivity spellItActivity;

	/**
	 * Basic constructor of class
	 * 
	 * @param activity
	 */
	public LetterBinAdapter(Activity activity) {
		this.context = activity;
		this.activity = activity;
		zoom_in = AnimationUtils.loadAnimation(context, R.anim.zoom_in);
		zoom_out = AnimationUtils.loadAnimation(context, R.anim.zoom_out);
		try {
			mLayoutInflater = (LayoutInflater) context
					.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
			mLetterBinList = new ArrayList<LetterBinModel>();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Add updated data on List
	 * 
	 */
	public void addUpdateDataIntoList(ArrayList<LetterBinModel> mData) {
		mLetterBinList = new ArrayList<LetterBinModel>();
		mLetterBinList.addAll(mData);
	}

	@Override
	public int getCount() {
		if (mLetterBinList != null) {
			return mLetterBinList.size();
		} else {
			return 0;
		}

	}

	@Override
	public LetterBinModel getItem(int position) {
		if (mLetterBinList != null) {
			return mLetterBinList.get(position);
		} else {
			return null;
		}
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = View.inflate(context.getApplicationContext(),
					R.layout.random_alphabet_bg, null);
			new ViewHolder(convertView);
		}
		ViewHolder holder = (ViewHolder) convertView.getTag();
		if (mLetterBinList != null) {
			final LetterBinModel letter_model = mLetterBinList.get(position);
			if (letter_model != null) {
				holder.random_alphabet.setText(letter_model.getLetter());
				if (letter_model.isEmpty()) {
					holder.random_alphabet.setVisibility(View.INVISIBLE);
				} else {
					holder.random_alphabet.setVisibility(View.VISIBLE);
					if (letter_model.isBlasted()) {
						holder.random_alphabet.setVisibility(View.INVISIBLE);
					}
				}
			}
		}
		return convertView;
	}

	/**
	 * List view row object and its views
	 * 
	 * @author Shruti
	 * 
	 */
	class ViewHolder {
		TextView random_alphabet;

		public ViewHolder(View view) {
			random_alphabet = (TextView) view.findViewById(R.id.letter_text);
			view.setTag(this);
		}
	}
}
