package com.biblezon.memorymatch.utils;

public interface OnRearrangeListener {
	
	public abstract void onRearrange(int oldIndex, int newIndex);
}
