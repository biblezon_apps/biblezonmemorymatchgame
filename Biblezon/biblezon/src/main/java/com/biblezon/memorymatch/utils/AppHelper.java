package com.biblezon.memorymatch.utils;


import com.biblezon.memorymatch.R;

/**
 * Application keys
 *
 * @author Anshuman
 */
public class AppHelper {

    public static String NAME_MATCH_WORD_EUCHARIST_CUP = "EUCHARIST CUP";
    public static String NAME_MATCH_WORD_CHALICE = "CHALICE";
    public static String NAME_MATCH_WORD_MONSTRANCE = "MONSTRANCE";
    public static String NAME_MATCH_WORD_ADVENT_WREATH = "ADVENT WREATH";
    public static String NAME_MATCH_WORD_HOST = "HOST";
    public static String NAME_MATCH_WORD_BAPTISMAL_FONT = "BAPTISMAL FONT";
    public static String NAME_MATCH_WORD_BIBLE = "BIBLE";
    public static String NAME_MATCH_WORD_JESUS = "JESUS";
    public static String NAME_MATCH_WORD_HOLY_MARY = "HOLY MARY";
    public static String NAME_MATCH_WORD_ANGELS = "ANGELS";
    public static String NAME_MATCH_WORD_ROSARY = "ROSARY";
    public static String NAME_MATCH_WORD_BAPTISM = "BAPTISM";
    public static String NAME_MATCH_WORD_CONFESSION = "CONFESSION";
    public static String NAME_MATCH_WORD_CONFESSIONAL = "CONFESSIONAL";
    public static String NAME_MATCH_WORD_PEWS = "PEWS";
    public static String NAME_MATCH_WORD_ALTAR = "ALTAR";
    public static String NAME_MATCH_WORD_ALTAR_SERVER = "ALTAR SERVER";
    public static String NAME_MATCH_WORD_ALTAR_BELLS = "ALTAR BELLS";
    public static String NAME_MATCH_WORD_HOLY_WATER_FONT = "HOLY WATER FONT";
    public static String NAME_MATCH_WORD_CRUCIFIX = "CRUCIFIX";
    public static String NAME_MATCH_WORD_LAST_SUPPER = "LAST SUPPER";
    public static String NAME_MATCH_WORD_EUCHARIST = "EUCHARIST";
    public static String NAME_MATCH_WORD_CONFIRMATION = "CONFIRMATION";
    public static String NAME_MATCH_WORD_ANOINTING_OF_THE_SICK = "ANOINTING OF THE SICK";
    public static String NAME_MATCH_WORD_MATRIMONY = "MATRIMONY";
    public static String NAME_MATCH_WORD_HOLY_ORDERS = "HOLY ORDERS";
    public static String NAME_MATCH_WORD_ASCENSION = "ASCENSION";
    public static String NAME_MATCH_WORD_CROSS = "CROSS";
    public static String NAME_MATCH_WORD_DOVE = "DOVE";
    public static String NAME_MATCH_WORD_HOLY_FAMILY = "HOLY FAMILY";
    public static String NAME_MATCH_WORD_HOLY_OILS = "HOLY OILS";
    public static String NAME_MATCH_WORD_LITURGICAL_CALENDAR = "LITURGICAL CALENDAR";
    public static String NAME_MATCH_WORD_MOTHER_THERESA = "MOTHER THERESA";
    public static String NAME_MATCH_WORD_OUR_LADY_OF_GUADALOUPE = "OUR LADY OF GUADALOUPE";
    public static String NAME_MATCH_WORD_PASCHAL_CANDLE = "PASCHAL CANDLE";
    public static String NAME_MATCH_WORD_POPE_FRANCIS = "POPE FRANCIS";

    /**
     * Catholic Images
     */
    public static int[] catholicImagesDrawableArray = {R.drawable.chalice,
            R.drawable.monstrance, R.drawable.advent_wreath, R.drawable.host,
            R.drawable.baptismal_font, R.drawable.paschal_candle,
            R.drawable.jesus, R.drawable.holy_mary, R.drawable.crucifix,
            R.drawable.rosary, R.drawable.baptism, R.drawable.confession, R.drawable.confessional,
            R.drawable.pews, R.drawable.altar, R.drawable.altar_server,
            R.drawable.altar_bells, R.drawable.holy_water_font,
            R.drawable.angels, R.drawable.last_supper, R.drawable.eucharist,
            R.drawable.confirmation, R.drawable.anointing_of_the_sick,
            R.drawable.matrimony, R.drawable.holy_orders, R.drawable.ascension,
            R.drawable.cross, R.drawable.dove, R.drawable.holy_family,
            R.drawable.holy_oils, R.drawable.liturgical_calendar,
            R.drawable.mother_theresa, R.drawable.our_lady_of_guadaloupe,
            R.drawable.bible, R.drawable.pope_francis, R.drawable.eucharist_cup};

    public static String[] catholicImagesArray = null;

    /**
     * Catholic Name
     */
    public static String[] catholicNameArray = {
            NAME_MATCH_WORD_CHALICE,
            NAME_MATCH_WORD_MONSTRANCE,
            NAME_MATCH_WORD_ADVENT_WREATH,
            NAME_MATCH_WORD_HOST,
            NAME_MATCH_WORD_BAPTISMAL_FONT,
            NAME_MATCH_WORD_PASCHAL_CANDLE,
            NAME_MATCH_WORD_JESUS,
            NAME_MATCH_WORD_HOLY_MARY,
            NAME_MATCH_WORD_CRUCIFIX,/* NAME_MATCH_WORD_PRIEST, */
            /* NAME_MATCH_WORD_CHASUBLE, */NAME_MATCH_WORD_ROSARY,
            /* NAME_MATCH_WORD_POPE, NAME_MATCH_WORD_NUN, */NAME_MATCH_WORD_BAPTISM,
            NAME_MATCH_WORD_CONFESSION, NAME_MATCH_WORD_CONFESSIONAL,
            NAME_MATCH_WORD_PEWS, NAME_MATCH_WORD_ALTAR,
            NAME_MATCH_WORD_ALTAR_SERVER, NAME_MATCH_WORD_ALTAR_BELLS,
            NAME_MATCH_WORD_HOLY_WATER_FONT, NAME_MATCH_WORD_ANGELS,
            NAME_MATCH_WORD_LAST_SUPPER, NAME_MATCH_WORD_EUCHARIST,
            NAME_MATCH_WORD_CONFIRMATION,
            NAME_MATCH_WORD_ANOINTING_OF_THE_SICK, NAME_MATCH_WORD_MATRIMONY,
            NAME_MATCH_WORD_HOLY_ORDERS, NAME_MATCH_WORD_ASCENSION,
            NAME_MATCH_WORD_CROSS, NAME_MATCH_WORD_DOVE,
            NAME_MATCH_WORD_HOLY_FAMILY, NAME_MATCH_WORD_HOLY_OILS,
            NAME_MATCH_WORD_LITURGICAL_CALENDAR,
            NAME_MATCH_WORD_MOTHER_THERESA,
            NAME_MATCH_WORD_OUR_LADY_OF_GUADALOUPE, NAME_MATCH_WORD_BIBLE,
            NAME_MATCH_WORD_POPE_FRANCIS, NAME_MATCH_WORD_EUCHARIST_CUP};

    // public static String[] catholicNameArray = null;
    /**
     * Size of Word Data
     */
    public static int sizeOfChatholicData = catholicNameArray.length;

    /**
     * One Second Time
     */
    public static int ONE_SECOND_TIME = 1000;
    public static int API_REQUEST_TIME = 20;

    public static String SPELL_IT = "Spell It";
    public static String TAP_IT = "Tap It";
    public static String MEMORY_MATCH = "Memory Match";
    public static String NAME_MATCH = "Name Match";
    public static String CHAPTER_MATCH = "Chapter Match";

    public static String HIGHEST_SCORE_IS = "HIGHEST SCORE IS:\n";
    public static String YOUR_SCORE_IS = "\nYour SCORE IS:\n";
    public static String HIGHEST_SCORE = "HIGHEST SCORE:\n";

    public static String FILE_FOLDER_NAME = "Biblezon_Game_app";

}
