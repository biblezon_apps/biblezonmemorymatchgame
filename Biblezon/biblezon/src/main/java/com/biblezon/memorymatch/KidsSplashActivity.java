package com.biblezon.memorymatch;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

public class KidsSplashActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);

		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				// move to DashBoard page ...
				startActivity(new Intent(KidsSplashActivity.this,
						DashBoard.class));
				// finish();
			}
		}, 1000);
	}

}
