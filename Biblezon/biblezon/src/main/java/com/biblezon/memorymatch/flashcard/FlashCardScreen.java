package com.biblezon.memorymatch.flashcard;

import java.util.ArrayList;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;

import com.biblezon.memorymatch.R;
import com.biblezon.memorymatch.control.HeaderViewManager;
import com.biblezon.memorymatch.control.PlaySound;
import com.biblezon.memorymatch.customview.CirclePageIndicator;
import com.biblezon.memorymatch.customview.PageIndicator;
import com.biblezon.memorymatch.flashcard.adapter.PageViewFragmentStatePagerAdapter;
import com.biblezon.memorymatch.model.CatholicWordsModel;
import com.biblezon.memorymatch.utils.AppHelper;

/**
 * Show product to user interaction screen In this screen showing product images
 * to user When user swipe over the product it will show next product of the
 * list
 * 
 * @author Anshuman
 *
 */
public class FlashCardScreen extends FragmentActivity {

	/**
	 * Custom List Data
	 */
	private ArrayList<CatholicWordsModel> mCatholicFlashCardArray = new ArrayList<CatholicWordsModel>();
	/**
	 * View pager GUI
	 */
	private ViewPager mPager;
	/**
	 * View page indicator
	 */
	private PageIndicator mIndicator;
	/**
	 * View pager fragment adapter
	 */
	private PageViewFragmentStatePagerAdapter mPagerAdapter;

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.support.v4.app.FragmentActivity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_product_screen);
		addDummyDataonGUI();
		initViews();
		headerViewManagement();

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		PlaySound.getInstance(this).startPlay();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		PlaySound.getInstance(this).stopPlay();
	}

	/**
	 * manages header view
	 */
	private void headerViewManagement() {
		HeaderViewManager.getInstance().InitializeHeaderView(
				FlashCardScreen.this);
		HeaderViewManager.getInstance().setHeading(
				true,
				FlashCardScreen.this.getResources().getString(
						R.string.chapter_match));
	}

	/**
	 * Add Dummy Data on GUI
	 */
	private void addDummyDataonGUI() {
		CatholicWordsModel wordsModel = new CatholicWordsModel();
		wordsModel.setCatholicWordsImage(R.drawable.altar);
		wordsModel.setCatholicWordsText(AppHelper.NAME_MATCH_WORD_ALTAR);
		mCatholicFlashCardArray.add(wordsModel);

		wordsModel = new CatholicWordsModel();
		wordsModel.setCatholicWordsImage(R.drawable.altar_bells);
		wordsModel.setCatholicWordsText(AppHelper.NAME_MATCH_WORD_ALTAR_BELLS);
		mCatholicFlashCardArray.add(wordsModel);

		wordsModel = new CatholicWordsModel();
		wordsModel.setCatholicWordsImage(R.drawable.altar_server);
		wordsModel.setCatholicWordsText(AppHelper.NAME_MATCH_WORD_ALTAR_SERVER);
		mCatholicFlashCardArray.add(wordsModel);

		wordsModel = new CatholicWordsModel();
		wordsModel.setCatholicWordsImage(R.drawable.angels);
		wordsModel.setCatholicWordsText(AppHelper.NAME_MATCH_WORD_ANGELS);
		mCatholicFlashCardArray.add(wordsModel);

		wordsModel = new CatholicWordsModel();
		wordsModel.setCatholicWordsImage(R.drawable.baptism);
		wordsModel.setCatholicWordsText(AppHelper.NAME_MATCH_WORD_BAPTISM);
		mCatholicFlashCardArray.add(wordsModel);

		wordsModel = new CatholicWordsModel();
		wordsModel.setCatholicWordsImage(R.drawable.bible);
		wordsModel.setCatholicWordsText(AppHelper.NAME_MATCH_WORD_BIBLE);
		mCatholicFlashCardArray.add(wordsModel);
	}

	/**
	 * InitView of the screen
	 */
	private void initViews() {
		// Get references to UI widgets
		mPager = (ViewPager) findViewById(R.id.pager);
		mIndicator = (CirclePageIndicator) findViewById(R.id.indicator);

		// Set adapter
		mPagerAdapter = new PageViewFragmentStatePagerAdapter(
				getSupportFragmentManager(), mCatholicFlashCardArray);
		mPager.setAdapter(mPagerAdapter);
		mIndicator.setViewPager(mPager);
		// Setup data on GUI

	}

}
