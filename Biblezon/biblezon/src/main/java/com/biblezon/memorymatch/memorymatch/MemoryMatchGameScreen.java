package com.biblezon.memorymatch.memorymatch;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;

import com.biblezon.memorymatch.R;
import com.biblezon.memorymatch.control.HeaderViewManager;
import com.biblezon.memorymatch.control.PlaySound;
import com.biblezon.memorymatch.memorymatch.tiles.EightByEightBoardFragment;
import com.biblezon.memorymatch.memorymatch.tiles.FourByFourBoardFragment;
import com.biblezon.memorymatch.memorymatch.tiles.SixBySixBoardFragment;
import com.biblezon.memorymatch.memorymatch.tiles.TwoByTwoBoardFragment;

public class MemoryMatchGameScreen extends FragmentActivity implements
		DifficultyDialog.OptionsFragmentInterface {

	private String difficulty;
	/**
	 * Debugging TAG
	 */
	private String TAG = MemoryMatchGameScreen.class.getSimpleName();
	Activity activity;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_game_screen);
		activity = this;
		headerViewManagement();
		showDifficultyDialog();
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		PlaySound.getInstance(this).startPlay();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		PlaySound.getInstance(this).stopPlay();
	}

	private void showDifficultyDialog() {
		DifficultyDialog difficultySelect = new DifficultyDialog();
		difficultySelect
				.show(getSupportFragmentManager(), DifficultyDialog.TAG);
	}

	/**
	 * manages header view
	 */
	private void headerViewManagement() {
		HeaderViewManager.getInstance().InitializeHeaderView(activity);
		HeaderViewManager.getInstance().setHeading(true,
				activity.getResources().getString(R.string.memory_match));
	}

	private void getGameBoard(String tag) {
		Fragment fragment = null;
		if (TwoByTwoBoardFragment.TAG.equals(tag)) {
			fragment = new TwoByTwoBoardFragment();
		} else if (FourByFourBoardFragment.TAG.equals(tag)) {
			fragment = new FourByFourBoardFragment();
		} else if (SixBySixBoardFragment.TAG.equals(tag)) {
			fragment = new SixBySixBoardFragment();
		} else if (EightByEightBoardFragment.TAG.equals(tag)) {
			fragment = new EightByEightBoardFragment();
		}

		if (fragment != null) {
			FragmentManager fm = getSupportFragmentManager();
			FragmentTransaction trans = fm.beginTransaction();
			trans.replace(R.id.gameScreenContainer, fragment, tag);
			trans.commit();
		}
	}

	@Override
	public void onOptionsFragmentResult(String difficulty) {
		Log.d(TAG, "difficulty :" + difficulty);
		if (getString(R.string.twobytwo).equals(difficulty)) {
			this.difficulty = TwoByTwoBoardFragment.TAG;
		} else if (getString(R.string.fourbyfour).equals(difficulty)) {
			this.difficulty = FourByFourBoardFragment.TAG;
		} else if (getString(R.string.sixbysix).equals(difficulty)) {
			this.difficulty = SixBySixBoardFragment.TAG;
		} else if (getString(R.string.eightbyeight).equals(difficulty)) {
			this.difficulty = EightByEightBoardFragment.TAG;
		} else {
			this.difficulty = TwoByTwoBoardFragment.TAG;
		}
		getGameBoard(this.difficulty);
	}

	public void showEndGameNavDialog(String time) {
		EndGameNavDialog navDialog = EndGameNavDialog.newInstance(time);
		navDialog.show(getSupportFragmentManager(), EndGameNavDialog.TAG);
	}

	@Override
	public void onBackPressed() {
		// super.onBackPressed();
		finish();
	}
}
