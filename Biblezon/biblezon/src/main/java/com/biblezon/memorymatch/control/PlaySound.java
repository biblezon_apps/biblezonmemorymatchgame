package com.biblezon.memorymatch.control;


import android.app.Activity;
import android.media.MediaPlayer;

import com.biblezon.memorymatch.R;

public class PlaySound {
	public static PlaySound mPlaySound;
	static Activity activity;
	MediaPlayer player;

	public static PlaySound getInstance(Activity mActivity) {
		try {
			activity = mActivity;
			if (mPlaySound == null) {
				mPlaySound = new PlaySound();
			}
		} catch (Exception e) {
			// TODO: handle exception
		}

		return mPlaySound;
	}

	public void startPlay() {
		try {
			// TODO Auto-generated method stub
			player = MediaPlayer.create(activity, R.raw.sound_1);
			// GlobalConfig.player.setLooping(true); // Set looping
			player.setVolume(40, 40);
			player.start();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

	}

	public void pauseSound() {
		try {
			if (player != null && player.isPlaying()) {
				player.pause();
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

	}

	public void resumeSound() {
		try {
			if (player != null) {
				player.start();
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

	}

	public void stopPlay() {
		try {
			if (player != null) {
				// GlobalConfig.player.release();
				player.stop();

			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

	}

}
