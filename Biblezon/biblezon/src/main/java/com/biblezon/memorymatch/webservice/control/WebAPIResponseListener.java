package com.biblezon.memorymatch.webservice.control;

/**
 * Web API Response Helper
 * 
 * @author Anshuman
 * 
 */
public interface WebAPIResponseListener {
	/**
	 * On Success of API Call
	 * 
	 * @param arguments
	 */
	void onSuccessOfResponse(Object... arguments);

	/**
	 * on Fail of API Call
	 * 
	 * @param arguments
	 */
	void onFailOfResponse(Object... arguments);

}
