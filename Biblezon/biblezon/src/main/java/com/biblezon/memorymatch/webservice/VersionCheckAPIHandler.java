package com.biblezon.memorymatch.webservice;

import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.os.Environment;
import android.provider.Settings;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request.Method;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.biblezon.memorymatch.application.AppApplicationController;
import com.biblezon.memorymatch.ihelper.GlobalKeys;
import com.biblezon.memorymatch.utils.AppDefaultUtils;
import com.biblezon.memorymatch.utils.AppHelper;
import com.biblezon.memorymatch.webservice.control.WebAPIResponseListener;
import com.biblezon.memorymatch.webservice.control.WebserviceAPIErrorHandler;
import com.biblezon.memorymatch.webservice.control.WebserviceResponseHandler;

/**
 * check version of application
 *
 * @author Shruti
 */
public class VersionCheckAPIHandler {
    private Activity mActivity;
    private Context context;
    /**
     * Debug TAG
     */
    private String TAG = VersionCheckAPIHandler.class.getSimpleName();
    /**
     * API Response Listener
     */
    private WebAPIResponseListener mResponseListener;
    DownloadManager downloadManager;
    private long downloadReference;

    /**
     * @param mActivity
     * @param webAPIResponseListener
     */
    public VersionCheckAPIHandler(Activity mActivity,
                                  WebAPIResponseListener webAPIResponseListener) {
        this.mActivity = mActivity;
        this.context = mActivity;
        this.mResponseListener = webAPIResponseListener;
        IntentFilter filter = new IntentFilter(
                DownloadManager.ACTION_DOWNLOAD_COMPLETE);
        mActivity.registerReceiver(downloadReceiver, filter);
        postAPICall();

    }

    /**
     * Making json object request
     */
    public void postAPICall() {
        /**
         * JSON Request
         */
        String version_url = (GlobalKeys.AUTO_UPDATE_URL + Settings.Secure
                .getString(mActivity.getContentResolver(),
                        Settings.Secure.ANDROID_ID)).trim();
        AppDefaultUtils.showLog(TAG, "Autoupdate : " + version_url);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Method.GET,
                version_url, "", new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                AppDefaultUtils.showInfoLog(TAG, "Response :"
                        + response);
                parseAPIResponse(response);
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                WebserviceAPIErrorHandler.getInstance()
                        .VolleyErrorHandler(error, mActivity);
                AppDefaultUtils.hideProgressDialog();
                mResponseListener.onSuccessOfResponse();
            }
        }) {

        };

        // Adding request to request queue
        AppApplicationController.getInstance().addToRequestQueue(jsonObjReq,
                GlobalKeys.VERSION_CHECK_KEY);
        // set request time-out
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                AppHelper.ONE_SECOND_TIME * AppHelper.API_REQUEST_TIME,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // Canceling request
        // MassAppApplicationController.getInstance().getRequestQueue()
        // .cancelAll(GlobalKeys.MASS_ACTIVITY_API);
    }

    /**
     * Parse Trip History API Response
     *
     * @param response
     */
    protected void parseAPIResponse(JSONObject response) {
        if (WebserviceResponseHandler.getInstance().checkVersionResponseCode(
                response)) {
            /* Success of API Response */
            try {
                float latest_version = Float.parseFloat(response
                        .getString(GlobalKeys.LATEST_VERSION));
                final String appURI = response
                        .getString(GlobalKeys.UPDATED_APP_URL);
                PackageInfo pInfo = null;
                try {
                    pInfo = mActivity.getPackageManager().getPackageInfo(
                            mActivity.getPackageName(), 0);
                } catch (NameNotFoundException e) {
                    e.printStackTrace();
                }
                float versionCode = Float.parseFloat(pInfo.versionName);

                if (latest_version > versionCode) {
                    // oh yeah we do need an upgrade, let the user know send
                    // an alert message
                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            mActivity);
                    builder.setMessage(
                            "There is newer version of this application available, click OK to upgrade now?")
                            .setPositiveButton("OK",
                                    new DialogInterface.OnClickListener() {
                                        // if the user agrees to upgrade
                                        public void onClick(
                                                DialogInterface dialog, int id) {
                                            // start downloading the file
                                            // using the download manager
                                            downloadManager = (DownloadManager) mActivity
                                                    .getSystemService(Context.DOWNLOAD_SERVICE);
                                            Uri Download_Uri = Uri
                                                    .parse(appURI);
                                            DownloadManager.Request request = new DownloadManager.Request(
                                                    Download_Uri);
                                            request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI);
                                            request.setAllowedOverRoaming(false);
                                            request.setTitle("Biblezon Games");
                                            request.setDestinationInExternalFilesDir(
                                                    mActivity,
                                                    Environment.DIRECTORY_DOWNLOADS,
                                                    GlobalKeys.APK_NAME
                                                            .replace("_", "")
                                                            + ".apk");
                                            downloadReference = downloadManager
                                                    .enqueue(request);
                                        }
                                    });
                    // show the alert message
                    builder.create().show();
                } else {
                    mResponseListener.onSuccessOfResponse();
                }

            } catch (Exception e) {
                e.printStackTrace();
                mResponseListener.onSuccessOfResponse();
            }

        } else {
			/* Fail of API Response */
            // AppDialogUtils.showMessageInfoWithOkButtonDialog(mActivity,
            // "Something Went wrong !!", null);
            mResponseListener.onSuccessOfResponse();
        }
    }

    // broadcast receiver to get notification about ongoing downloads
    private BroadcastReceiver downloadReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            // check if the broadcast message is for our Enqueued download
            long referenceId = intent.getLongExtra(
                    DownloadManager.EXTRA_DOWNLOAD_ID, -1);
            if (downloadReference == referenceId) {

                Log.v("VersionCheckAPIHandler",
                        "Downloading of the new app version complete");
                // start the installation of the latest version
                Intent installIntent = new Intent(Intent.ACTION_VIEW);
                installIntent.setDataAndType(downloadManager
                                .getUriForDownloadedFile(downloadReference),
                        "application/vnd.android.package-archive");
                installIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mActivity.startActivity(installIntent);

            }
        }
    };
}
